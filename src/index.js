import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

firebase.initializeApp(
{
	apiKey: "AIzaSyB_fl3SubidvRVwXDLe5HTcpuWgj46e6nI",
    authDomain: "mitienda-2cf28.firebaseapp.com",
    databaseURL: "https://mitienda-2cf28.firebaseio.com",
    projectId: "mitienda-2cf28",
    storageBucket: "mitienda-2cf28.appspot.com",
    messagingSenderId: "920626282896"	
}
);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
